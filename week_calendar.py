import tkinter as tk
import tkinter.font as font
import datetime
import os

# personal class
from notebook import Note


class Week_calendar(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self)
        self.fileDir = os.path.dirname(os.path.abspath(__file__))
        self.directory = self.fileDir + '/event_files'
        if not os.path.isdir(self.directory):
            os.mkdir(self.directory)
        # pathlib.Path(self.directory).mkdir(parents=True, exist_ok=True)
        self.my_font = font.Font(size=15)
        # Resize the buttons and labels
        self.top = self.winfo_toplevel()
        self.top.rowconfigure(0, weight=1)
        self.top.columnconfigure(0, weight=1)
        self.grid(sticky="nsew")
        self._week_frame = tk.Frame(self)
        self._week_frame.rowconfigure(0, weight=1)
        self._week_frame.rowconfigure(1, weight=1)
        self._week_frame.rowconfigure(2, weight=1)
        self._week_frame.rowconfigure(3, weight=20)
        # Button to change the week
        self.previous_button = tk.Button(
            self._week_frame, font=self.my_font, text="<< previous", width=10, command=self.previous_week)
        self.previous_button.grid(row=0, column=0, sticky="sew")
        self.current_button = tk.Button(
            self._week_frame, font=self.my_font, text="current", width=10, command=self.current_week)
        self.current_button.grid(row=0, column=3, sticky="sew")
        self.next_button = tk.Button(
            self._week_frame, font=self.my_font, text="next >>", width=10, command=self.next_week)
        self.next_button.grid(row=0, column=6, sticky="sew")
        self.today = datetime.date.today()
        # find the monday with the corresponding number of the day date
        self.monday = self.today - \
            datetime.timedelta(days=self.today.weekday())
        self.date = self.today.strftime("%d-%m-%Y")
        self._day_names = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        # Empty tables for the different column of the week calendar
        self._week_day_name_array = []
        self._day_date_array = []
        self._event_array = []
        for i in range(7):
            # Make the column with the name of the day
            self._week_name_label, self._week_day_name_array = self.label_calendar(
                self._week_day_name_array, i, 1, None)
            d = self._day_names[i]
            self._week_name_label.configure(text=d)
            # date corresponding to each day of the week
            self.number = self.monday + datetime.timedelta(days=i)
            day = self.number.strftime("%d/%m/%Y")
            # make a column with the day-month-year corresponding to the day name
            self._day_date_label, self._day_date_array = self.label_calendar(
                self._day_date_array, i, 2, None)
            self._day_date_label.configure(text=day)
            # make a column for the event & meeting ...
            self._event_label, self._event_array = self.label_calendar(
                self._event_array, i, 3, 'groove')
            tk.Grid.columnconfigure(self._week_frame, i, weight=1)
            # name of the file corresponding to the ligne
            self.filename = self.name(i)
            self.set_label()
            index = len(self._event_array)
            # Open a window when you click on the date label
            self._event_label.bind("<Button-1>", lambda event,
                                   i=(index - 1): self.create_window(i))
            self.highlight(index - 1)
        self._week_frame.pack(fill="both", expand=True)

    def label_calendar(self, _array, index, row_position, add_relief):
        """ Create the differents column and row in the frame.
        _array = create an array with the date needed (in this code, day of week, date, event)
        index = define the number of rows (in this code, from the for loop),
        column_position = add a column by choosinf it position number
        """
        _label = tk.Label(self._week_frame, width=10, font=self.my_font, relief='solid')
        _array.append(_label)
        _array[-1].grid(row=row_position, column=index, sticky="nsew", pady=(0, 1))
        return _label, _array

    def name(self, index):
        """ Create the name of the file with the date.
        index = have the name corresponding to the row (in this code the date) """
        self.column = self._day_date_array[index]
        self.column_text = self.column.cget('text') + ".txt"
        self.filename = self.column_text.replace("/", "-")
        return self.filename

    def set_label(self):
        """ Initialize, show the text in the label for the current week,
        used only once, when the code is opened (rephrase)"""
        PATH = self.directory + '/' + self.filename
        if os.path.isfile(PATH):
            self.my_note = open(PATH, 'r')
            self.text_note = self.my_note.read()
            self._event_label.configure(text=self.text_note)
            self.my_note.close()

    def event_update_label(self, filename, index):
        """ Update the label corresponding to the date, show the different task/event if there is one """
        PATH = self.directory + '/' + filename
        if os.path.isfile(PATH):
            self.my_note = open(PATH, 'r')
            self.text_note = self.my_note.read()
            self._event_array[index].configure(text=self.text_note)
            self.my_note.close()
        else:
            self._event_array[index].configure(text='')

    def sorted_monday_date(self):
        """ get the date from the label and change for a datetime date format """
        self.column = self._day_date_array[0]
        self.x_monday = self.column.cget('text')
        split_date = self.x_monday.split("/")
        reverse_date = split_date[::-1]
        s = '-'
        join_date = s.join(reverse_date)
        sorted_date = datetime.datetime.strptime(join_date, '%Y-%m-%d')
        return sorted_date

    def highlight(self, index):
        self.column = self._day_date_array[index]
        self.column_text = self.column.cget('text')
        label_date = self.column_text.replace("/", "-")
        if self.date == label_date:
            self._day_date_array[index].configure(background="gray")
        else:
            self._day_date_array[index].configure(background="#d9d9d9")

    def previous_week(self):
        """ Show the previous weeks """
        self.first_day = self.sorted_monday_date()
        for index in range(7):
            self.number = self.first_day - \
                datetime.timedelta(weeks=1) + datetime.timedelta(days=index)
            previous_monday = self.number.strftime("%d/%m/%Y")
            previous_m = previous_monday.replace("/", "-") + ".txt"
            self._day_date_array[index].configure(text=previous_monday)
            self.event_update_label(previous_m, index)
            self.highlight(index)

    def current_week(self):
        """ Show the current week"""
        for index in range(7):
            self.number = self.monday + datetime.timedelta(days=index)
            current_monday = self.number.strftime("%d/%m/%Y")
            current_m = current_monday.replace("/", "-") + ".txt"
            self._day_date_array[index].configure(text=current_monday)
            self.event_update_label(current_m, index)
            self.highlight(index)

    def next_week(self):
        """Show the next week """
        self.first_day = self.sorted_monday_date()
        for index in range(7):
            self.number = self.first_day + \
                datetime.timedelta(weeks=1) + datetime.timedelta(days=index)
            next_monday = self.number.strftime("%d/%m/%Y")
            next_m = next_monday.replace("/", "-") + ".txt"
            self._day_date_array[index].configure(text=next_monday)
            self.event_update_label(next_m, index)
            self.highlight(index)

    def create_window(self, index):
        """ Create a top level window (pop up) to write and save an event, task ... """
        self.event = tk.Toplevel()
        self.event.wait_visibility()
        self.event.grab_set()
        self.event.attributes("-topmost", True)
        # show the date to add the event
        self.current_date = tk.Label(self.event, font=self.my_font, text=self.title(index))
        self.current_date.grid(row=0, column=0)
        self._event_textbox = tk.Text(self.event, font=self.my_font, height=10, width=20)
        self._event_textbox.grid(row=1, column=0)
        self.filename = self.name(index)
        self.note = Note(self._event_textbox, self.filename, self.directory)
        self.note.verify_note()
        path = self.directory + "/" + self.filename
        print(path)
        self.save_button = tk.Button(self.event, font=self.my_font, text="save", width=10,
                                     command=lambda: self.close())
        self.save_button.grid(row=2, column=0)
        self.save_button.bind(
            "<Button-1>", lambda event:
                self.update_label(self._event_textbox, path, index))

    def title(self, index):
        """ Set the date as title at the top of the pop up window event """
        self.column = self._day_date_array[index]
        self.current_day_date = self.column.cget('text')
        return self.current_day_date

    def update_label(self, textbox, filename, index):
        """ Update the label of the week calendar with the text of the pop up window event """
        self.my_note = open(filename, 'w')
        self.text_note = textbox.get("1.0", "end")
        self._event_array[index].configure(text=self.text_note)
        self.my_note.close()

    def close(self):
        """ Close the pop up window event """
        self.note.save_note()
        self.event.quit
        self.event.destroy()


# Uncomment to display the window
# class MainW(tk.Tk):
#     def __init__(self, parent):
#         tk.Tk.__init__(self, parent)
#         self.parent = parent
#         self.window = Week_calendar()
#         self.window.grid(row=0, column=10, rowspan=2)
#
#
# if __name__ == "__main__":
#     app = MainW(None)
#     app.mainloop()
