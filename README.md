# tkinter_agenda

A raspberry project.

equipment:
- raspberry pi3 ModelB+
- Touchscreen

At the top there is the time and date, a week calendar and on the right a to do list. 
With a click under the date it open a window, you can add an event/task.  
It is possible to use the week_calendar and the todolist on their own.  
To do this, you have to remove the commented part at the end the files.  

![alt text](images/my_agenda.png "Tkinter agenda")

![alt text](images/my_agenda_event.png "add an event/task")

## Usage

```bash
python my_agenda.py
```

