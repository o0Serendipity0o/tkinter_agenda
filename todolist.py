import tkinter as tk
import tkinter.font as font

import os


class To_do_list(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self)
        # font size
        self.my_font = font.Font(size=15)
        # configure
        self.top = self.winfo_toplevel()
        self.grid(sticky="nsew")
        # create, open, read the file todolist.txt
        self.fileDir = os.path.dirname(os.path.abspath(__file__))
        PATH = self.fileDir + '/' + 'todolist.txt'
        # Initialisation variable
        global is_cross
        is_cross = False
        # Widgets
        self.title = tk.Label(self, font=18, text='To Do:')
        self.title.pack(side='top', anchor='nw')
        self.todolist_entry = tk.Entry(self, font=self.my_font, width=20)
        self.todolist_entry.pack(anchor='nw')
        self.todolist_frame = tk.Frame(self)
        self.todolist_frame.rowconfigure(1, weight=1)
        self.todolist_frame.pack(fill='both', expand=True)
        self.todolist = tk.Listbox(self.todolist_frame, font=self.my_font, width=20, height=13)
        self.todolist.pack(side='left', fill='both', expand=True)
        self.todolist_scrollbar = tk.Scrollbar(self.todolist_frame)
        self.todolist_scrollbar.pack(side='right', fill='both', expand=True)
        self.todolist.config(yscrollcommand=self.todolist_scrollbar.set)
        self.todolist_scrollbar.config(command=self.todolist.yview)
        # Verify if the file does exist
        if os.path.isfile(PATH):
            file = open('todolist.txt', "r")
            self.list_item = file.readlines()
            for item in self.list_item:
                self.todolist.insert('end', item.strip())
            file.close()
        # binding the events
        self.todolist_entry.bind('<Return>', lambda event: self.add_item())
        self.todolist_entry.bind('<KP_Enter>', lambda event: self.add_item())
        self.todolist.bind('<BackSpace>', lambda event: self.delete_item())
        self.todolist.bind('<Delete>', lambda event: self.delete_item())
        self.todolist.bind('<Double-Button-1>', lambda event: self.un_cross_item())

    def add_item(self):
        i = self.todolist.curselection()
        item = self.todolist_entry.get()
        if len(i) == 0:
            self.todolist.insert('end', item)
        else:
            self.todolist.insert(i[0]+1, item)
            print(self.todolist.curselection())
        self.save_note()

    def delete_item(self):
        self.todolist.delete('anchor')
        self.save_note()

    def save_note(self):
        """Save a note"""
        with open('todolist.txt', "w") as f:
            for i in self.todolist.get(0, "end"):
                f.write(i + "\n")
        f.close()

    def cross_off_item(self):
        # Cross off item
        self.todolist.itemconfig(self.todolist.curselection(), fg="#dedede")
        # Get rid of selection bar
        self.todolist.selection_clear(0, 'end')

    def uncross_item(self):
        # Cross off item
        self.todolist.itemconfig(self.todolist.curselection(), fg="#000000")
        # Get rid of selection bar
        self.todolist.selection_clear(0, 'end')

    def un_cross_item(self):
        global is_cross
        if is_cross:
            is_cross = False
            self.cross_off_item()
        else:
            is_cross = True
            self.uncross_item()

    def delete_crossed(self):
        count = 0
        while count < self.todolist.size():
            if self.todolist.itemcget(count, "fg") == "#dedede":
                self.todolist.delete(self.todolist.index(count))
            count += 1
        self.save_note()


# Uncomment to display the window
# class MainW(tk.Tk):
#     def __init__(self, parent):
#         tk.Tk.__init__(self, parent)
#         self.parent = parent
#         # self.geometry("500x500")
#         self.window = To_do_list()
#         self.window.grid(row=0, column=10, rowspan=2)
#
#
# if __name__ == "__main__":
#     app = MainW(None)
#     app.mainloop()
