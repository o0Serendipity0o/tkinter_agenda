import tkinter as tk
import tkinter.font as font
from time import strftime
from datetime import date

from week_calendar import Week_calendar
from todolist import To_do_list


class MainW():
    def __init__(self, window):
        self.window = window
        self.my_font = font.Font(size=15)
        self.window.title("Agenda")
        # Configuration row and column to resize correctly
        self.window.rowconfigure(1, weight=40)  # big to have the week_calendar taking a maximum of space
        self.window.columnconfigure(0, weight=1)
        self.window.columnconfigure(1, weight=1)
        # Label date and time
        self.date_label = tk.Label(self.window, font=self.my_font)
        self.time_label = tk.Label(self.window, font=self.my_font)
        # class calendar and to do list
        self.week_calendar = Week_calendar()
        self.todolist = To_do_list()
        # Position of the widgets on the windows
        self.date_label.grid(row=0, column=0, padx=5, sticky="nse")
        self.time_label.grid(row=0, column=1, padx=5, sticky="nsw")
        self.week_calendar.grid(row=1, column=0, columnspan=2, padx=5, pady=5, sticky="nsew")
        self.todolist.grid(row=0, column=2, rowspan=2, padx=5, pady=5, sticky="nsew")
        # show the date and time
        self.today_date()
        self.digitalclock()

    def today_date(self):
        """ Show the today date in a label """
        self.day = date.today()
        self.today = self.day.strftime("%d/%m/%Y")
        self.date_label.config(text=str(self.today))

    def digitalclock(self):
        """Show the time in a label"""
        string = strftime('%H:%M:%S %p')
        self.time_label.config(text=string)
        self.time_label.after(1000, self.digitalclock)

    def quit(self):
        self.window.quit
        self.window.destroy()


def main():
    root = tk.Tk()
    app = MainW(root)
    root.mainloop()


if __name__ == '__main__':
    main()
