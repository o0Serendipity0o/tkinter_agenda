# Notes - memo, works with tkinter and the Texbtox
import os


class Note:
    def __init__(self, textbox, filename, fileDir):
        self.textbox = textbox
        self.filename = filename
        self.fileDir = fileDir

    def verify_note(self):
        """Verify if the note does existe, if not create a new one"""
        self.PATH = self.fileDir + '/' + self.filename
        if os.path.isfile(self.PATH):
            self.read_note()

    def read_note(self):
        """Read a note which does exist and show in a text box"""
        self.my_note = open(self.PATH, 'r')
        text = self.my_note.read()
        self.textbox.insert("end", text)
        self.my_note.close()

    def save_note(self):
        """Save a note from a text box"""
        self.my_note = open(self.PATH, 'w')
        text = self.textbox.get("1.0", "end")
        self.my_note.write(text)
        self.my_note.close()
